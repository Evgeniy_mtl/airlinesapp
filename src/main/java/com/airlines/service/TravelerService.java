package com.airlines.service;

import com.airlines.entity.Traveler;
import com.airlines.repository.TravelerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TravelerService implements ITravelerService {

    @Autowired
    TravelerRepo travelerRepo;

    @Override
    public void createTraveler(Traveler traveler) {
        travelerRepo.saveAndFlush(traveler);
    }
}
