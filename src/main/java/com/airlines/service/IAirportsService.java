package com.airlines.service;

import com.airlines.entity.Destination;

import java.util.List;

public interface IAirportsService {
    public List<Destination> getAllAirports();
}
