package com.airlines.service;

import com.airlines.entity.Ticket;

import java.util.List;

public interface ITicketService {

    void createTicket (final Ticket ticket);
    List<Ticket> getAllTickets ();
}
