package com.airlines.service;

import com.airlines.entity.Traveler;

public interface ITravelerService {

    void createTraveler (Traveler traveler);
}
