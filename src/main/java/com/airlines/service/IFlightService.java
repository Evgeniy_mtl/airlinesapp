package com.airlines.service;

import com.airlines.entity.Flight;

public interface IFlightService {

    void createFlight (final Flight flight);
}
