package com.airlines.repository;

import com.airlines.entity.Traveler;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TravelerRepo extends JpaRepository<Traveler, Long> {
}
