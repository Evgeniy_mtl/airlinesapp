package com.airlines.repository;

import com.airlines.entity.Flight;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FlightRepo extends JpaRepository <Flight, Long> {
}
