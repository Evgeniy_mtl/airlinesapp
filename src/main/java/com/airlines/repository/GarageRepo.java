package com.airlines.repository;

import com.airlines.entity.Garage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GarageRepo extends JpaRepository<Garage, Integer> {
}
