package com.airlines.entity.client;

import com.airlines.entity.Destination;

import java.util.List;

public interface IAirportClient {

    List<Destination> getAllAirports();
}
