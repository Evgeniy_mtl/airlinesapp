package com.airlines.controllers;

import com.airlines.entity.Traveler;
import com.airlines.service.TravelerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TravelerController {

    @Autowired
    TravelerService travelerService;

    @RequestMapping(path = "/travelers", method = RequestMethod.POST)
    public void createTraveler(@RequestBody Traveler traveler){
        travelerService.createTraveler(traveler);
    }

}
