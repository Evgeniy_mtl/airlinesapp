package com.airlines.controllers;

import com.airlines.entity.Aircraft;
import com.airlines.entity.Garage;
import com.airlines.service.AircraftService;
import com.airlines.service.GarageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class GarageController {

    @Autowired
    private GarageService garageService;

    @Autowired
    private AircraftService aircraftService;

    @RequestMapping(path = "/garages", method = RequestMethod.GET)
    public List<Garage> getGarage(){
        return garageService.getAllGarages();
    }
    @RequestMapping(path = "/garages", method = RequestMethod.POST)
    public void createGarage(@RequestBody Garage garage){
        garageService.createGarage(garage);
    }
    @RequestMapping(path = "/garages{id}", method = RequestMethod.DELETE)
    public void deleteGarage(@RequestParam int id){
        garageService.deleteGarage(id);
    }
    @RequestMapping(path = "/garages", method = RequestMethod.PUT)
    public void updateGarage(@RequestBody Garage garage){
        garageService.updateGarage(garage);
    }
    @RequestMapping(path = "/garages/{garageId}/{planeId}", method = RequestMethod.PUT)
    public void parkPlaneToGarage(@PathVariable("garageId")int garageId,@PathVariable("planeId")int planeId){
        final Optional<Garage> garage = garageService.getGarageById(garageId);
        final Optional<Aircraft> aircraft = aircraftService.getAircraftById(planeId);
        garageService.addPlaneToGarage(garage.get(),aircraft.get());
    }
}
