package com.airlines.mapper;

import com.airlines.entity.Destination;
import generated.restclient.model.AirportGen;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface IAirportMapper {

    Destination convertToDestination(AirportGen airport);
}
