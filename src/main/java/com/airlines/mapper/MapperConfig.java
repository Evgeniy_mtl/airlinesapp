package com.airlines.mapper;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackageClasses = {MapperConfig.class})
public class MapperConfig {
}
